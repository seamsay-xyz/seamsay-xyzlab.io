using Dates: Dates
using Luxor: Luxor
using PlotlyJS: PlotlyJS
using UUIDs: UUIDs

function logo(file = "_assets/logo.svg")
    size_x = 600.0
    size_y = 600.0

    spacing_x = size_x / 4
    spacing_y = size_y / 5

    offset_y = spacing_y / 2

    mkpath(dirname(file))
    Luxor.Drawing(size_x, size_y, file)
    Luxor.background(1.0, 1.0, 1.0, 1.0)
    Luxor.origin()

    for i = -1:1
        for j = -1:2:1
            let x = i * spacing_x, y1 = j * offset_y, y2 = j * (offset_y + spacing_y)
                Luxor.line(Luxor.Point(x, y1), Luxor.Point(x, y2), :stroke)
            end
        end
    end

    for i = 0:1
        let x1 = (-1 + i) * spacing_x, x2 = i * spacing_x, y1 = -offset_y, y2 = offset_y
            Luxor.line(Luxor.Point(x1, y1), Luxor.Point(x2, y2), :stroke)
        end
    end

    for i = -1:2:1
        for j = -1:2:1
            let x1 = 0,
                y1 = j * (offset_y + 2 * spacing_y),
                x2 = spacing_x * i,
                y2 = j * (offset_y + spacing_y)

                Luxor.line(Luxor.Point(x1, y1), Luxor.Point(x2, y2), :stroke)
            end
        end
    end

    Luxor.line(Luxor.Point(-spacing_x, offset_y), Luxor.Point(-spacing_x / 2, 0), :stroke)
    Luxor.line(Luxor.Point(spacing_x / 2, 0), Luxor.Point(spacing_x, -offset_y), :stroke)

    Luxor.finish()
end

function _pagenav(io, root, pages, level, base = "")
    indent = "  "

    println(io, indent^level, "<ul>")

    for entry in sort(collect(keys(pages)))
        stem, extension = splitext(entry)
        path = joinpath(root, entry)

        if isnothing(pages[entry]) && (stem == "index" || extension != ".md")
            continue
        end

        if isnothing(pages[entry])
            url = base * "/" * first(splitext(entry))
            title = pagevar(path, "title")
            link = """<a href="$url">$title</a>"""

            println(
                io,
                indent^(level + 1),
                """<li class="sidebar-nav-item{{ispage $url}} active{{end}}">$link</li>""",
            )
        else
            children = pages[entry]
            url = base * "/" * entry

            link = if any(==("index.md"), keys(children))
                title = pagevar(joinpath(path, "index.md"), "title")
                """<a href="$url">$title</a>"""
            else
                titlecase(replace(entry, "--" => "-", '-' => ' '))
            end

            if any(!=("index.md"), keys(children))
                println(io, indent^(level + 1), "<li>")

                println(io, indent^(level + 2), """<input type="checkbox" id="$url">""")
                println(
                    io,
                    indent^(level + 2),
                    """<label for="$url" class="sidebar-nav-item{{ispage $url/*}} active{{end}}">$link</label>""",
                )

                _pagenav(io, path, children, level + 2, url)

                println(io, indent^(level + 1), "</li>")
            else
                println(
                    io,
                    indent^(level + 1),
                    """<li class="sidebar-nav-item{{ispage $url}} active{{end}}">$link</li>""",
                )
            end
        end
    end

    println(io, indent^level, "</ul>")
end

function hfun_pagenav()
    # TODO: Use Franklin's logic for this.
    ignore = chopsuffix.(getgvar(:ignore; default = []), "/")

    pages = Dict()
    for (root, _, files) in walkdir(".")
        root = normpath(root)
        valid = filter(files) do file
            _, extension = splitext(file)
            extension == ".md" && !(file in ("404.md", "config.md"))
        end
        (isempty(valid) || any(i -> startswith(root, i), ignore)) && continue

        dict = pages
        for segment in splitpath(normpath(root))
            segment == "." && continue
            if !haskey(dict, segment)
                dict[segment] = Dict()
            end
            dict = dict[segment]
        end

        for file in valid
            dict[file] = nothing
        end
    end

    io = IOBuffer()
    _pagenav(io, "", pages, 2)

    html2(String(take!(io)), cur_lc())
end

function _git_time(filter)
    rpath = get_rpath()

    # TODO: Use LibGit2 for this.
    unix_string = readchomp(`git log --pretty=format:%at $filter $rpath`)

    if isempty(unix_string)
        @warn "No git date found for file." rpath filter
        "Unknown"
    else
        unix_int = parse(Int, unix_string)
        @debug "Git date found for file." rpath filter timestamp = unix_int
        # TODO: Shouldn't Franklin handle this?
        Dates.format(Dates.unix2datetime(unix_int), getgvar(:date_format))
    end
end

function hfun_git_ctime()
    _git_time("--diff-filter=A")
end

function hfun_git_mtime()
    _git_time("--max-count=1")
end

function _plot_plotly(id, url)
    """
        <div id="$id"></div>
        <script type="module">
            import '/libs/plotly.js';
            plot(document.getElementById("$id"), '$url');
        </script>
    """
end

function html_show(plot::PlotlyJS.SyncPlot)
    id = "plot-$(UUIDs.uuid4())"

    rel_url = joinpath("assets", "plotly", dirname(get_rpath()), id * ".json")
    path = sitepath(rel_url)
    url = "/" * rel_url

    mkpath(dirname(path))
    PlotlyJS.savejson(plot, path)

    _plot_plotly(id, url)
end

function lx_plotlyfig(p::Vector{String})::String
    id = "plot-$(UUIDs.uuid4())"
    path = p[1]
    _plot_plotly(id, path)
end
