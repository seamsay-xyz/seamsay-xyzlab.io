+++
title = "Welcome!"
+++

Hello!
My name is Sean Marshallsay, and this is my personal website.
On here you'll find my [blog](/blog), some of [my favourite recipes](/cooking), and others!
I mostly talk about maths, physics, and scientific software development, but I will on occasion talk about other things too.
Feel free to get in touch via any of my contact links at the bottom of the page[^1].

# Who Am I?

I am currently a PhD candidate in the [Centre For Light-Matter Interactions](https://www.qub.ac.uk/research-centres/light-matter-interactions/) at [Queen's University Belfast](https://www.qub.ac.uk/), working on the [R-Matrix With Time-Dependence](https://uk-amor.gitlab.io/RMT/rmt/) code for simulating the full multi-electron dynamics of atoms and molecules in LASER fields.
I have a varied history in scientific software development, however, with some of my previous areas of work including predictive algorithms for sports betting, and algorithms for leak detection, and PRV operation in water networks.

[^1]: Though I can't promise I'll respond in a timely manner 🙈
