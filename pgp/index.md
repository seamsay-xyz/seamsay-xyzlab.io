+++
title = "PGP Keys"
+++

# PGP Keys

Here you can find my PGP Public Keys.
They have been uploaded to <https://keys.openpgp.org> and are also available to download from this site (see links below).

## Personal

This is my personal key that I will use for the majority of signing and encryption.

\note{Not all machines from which I make Git commits will have GPG available[^lack], and so a lack of this key in of itself should not cause suspicion.}

```text
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEZgkmzhYJKwYBBAHaRw8BAQdAvjlWAPPD+knddFhPw3JB+oKwQdF+YvMU5gg8
VCu+KQe0JVNlYW4gTWFyc2hhbGxzYXkgPHNybS4xNzA4QGdtYWlsLmNvbT6IjgQT
FgoANhYhBMo8eiCEU0EJhmpjuj0/LjytpnwkBQJmCSbOAhsDBAsJCAcEFQoJCAUW
AgMBAAIeBQIXgAAKCRA9Py48raZ8JBozAQCThDELrt86VpqSnesgGVkbucrKEFK3
kqwZPiJ5n7rYhwD9EmVJ2ph4qZp3VH6Zhk6oLn4y44+dISI+8ETedFk8UAq0K1Nl
YW4gTWFyc2hhbGxzYXkgPHNtYXJzaGFsbHNheTAxQHF1Yi5hYy51az6IjgQTFgoA
NhYhBMo8eiCEU0EJhmpjuj0/LjytpnwkBQJmCU20AhsDBAsJCAcEFQoJCAUWAgMB
AAIeBQIXgAAKCRA9Py48raZ8JPS7AQCNMlE3VB+f827APd1Xsn8aWOYQB6MaQTd2
dYwyuOGBMwD9G4TeOxCI4tiOryiz9cRXHewVtocl49Ox7DOYNzgUKA+4OARmCSbO
EgorBgEEAZdVAQUBAQdAISDouE/lCT6Xln3Tq6KD3znhrkw2IK2awAh5BqCTZQ4D
AQgHiHgEGBYKACAWIQTKPHoghFNBCYZqY7o9Py48raZ8JAUCZgkmzgIbDAAKCRA9
Py48raZ8JHblAP92LvjCV9mguw7GIoZadXBuu/zExoGKgxUmjhl52qJj8QEA5Y2U
AjlgRmA6i7IMmCcnM2Nk1hutZkiGLrVP4r1lfQM=
=JzTk
-----END PGP PUBLIC KEY BLOCK-----
```

The key can be downloaded [with this link](SeanMarshallsay-Personal-3D3F2E3CADA67C24.asc).

[^lack]: And sometimes I won't be using a machine often enough to warrant setting up GPG on it.
