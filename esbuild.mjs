import * as esbuild from 'esbuild';

await esbuild.build({
    entryPoints: ['./libs/highlight.mjs', './libs/katex.mjs', './libs/plotly.mjs'],
    outdir: '_libs',
    bundle: true,
});

await esbuild.build({
    entryPoints: ['./css/highlight.css', './css/katex.css'],
    outdir: '_css',
    bundle: true,
    loader: {
        '.ttf': 'file',
        '.woff': 'file',
        '.woff2': 'file',
    }
});