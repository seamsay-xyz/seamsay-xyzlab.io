import hljs from 'highlight.js/lib/core';

import json from 'highlight.js/lib/languages/json';
import julia from 'highlight.js/lib/languages/julia';
import julia_repl from 'highlight.js/lib/languages/julia-repl';
import python from 'highlight.js/lib/languages/python';
import python_repl from 'highlight.js/lib/languages/python-repl';
import r from 'highlight.js/lib/languages/r';
import shell from 'highlight.js/lib/languages/shell';

hljs.registerLanguage('json', json);
hljs.registerLanguage('julia', julia);
hljs.registerLanguage('julia-repl', julia_repl);
hljs.registerLanguage('python', python);
hljs.registerLanguage('python-repl', python_repl);
hljs.registerLanguage('r', r);
hljs.registerLanguage('shell', shell);

document.addEventListener("DOMContentLoaded", function (_) {
    hljs.configure({ tabReplace: '    ' });
    hljs.highlightAll();
});
