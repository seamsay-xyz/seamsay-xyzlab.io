import renderMathInElement from 'katex/dist/contrib/auto-render.mjs';

document.addEventListener(
    "DOMContentLoaded",
    function (_) {
        renderMathInElement(document.body);
    }
)