import Plotly from 'plotly.js-dist/plotly';

export async function plot(div, url) {
    const response = await fetch(url);
    const fig = await response.json();

    // See the documentation of Plotly JS: https://plotly.com/javascript/responsive-fluid-layout/
    if (typeof fig.config === 'undefined') {
        fig["config"] = {};
    }
    delete fig.layout.width;
    delete fig.layout.height;
    fig["layout"]["autosize"] = true;
    fig["config"]["autosizable"] = true;
    fig["config"]["responsive"] = true;

    // Make it easier to scroll through the website rather than being blocked by a figure.
    fig.config["scrollZoom"] = false;

    // By default `PlotlyJS.savejson` adds attributes to make a static plot, disable them to make the website fancier.
    delete fig.config.staticPlot;
    delete fig.config.displayModeBar;
    delete fig.config.doubleClick;
    delete fig.config.showTips;

    Plotly.newPlot(div, fig);
}

// TODO: wtf?!
window.plot = plot;
window.Plotly = Plotly;
