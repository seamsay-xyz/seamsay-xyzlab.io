+++
author = "Sean Marshallsay"

site_title = "seamsay.xyz"
description = "Musings on science, software, and cooking."
website_url = "https://site.seamsay.xyz/"

rss_website_title = site_title
rss_website_descr = description
rss_website_url = website_url

date_format = "dd U yyyy"
fn_title = "Footnotes"
mintoclevel = 2

ignore = [
    ".JuliaFormatter.toml",
    ".direnv/",
    ".envrc",
    ".git/",
    ".gitignore",
    ".gitlab-ci.yml",
    ".venv/",
    ".vscode/",
    "Manifest.toml",
    "Project.toml",
    "css/",
    "esbuild.mjs",
    "flake.lock",
    "flake.nix",
    "libs/",
    "node_modules/",
    "package-lock.json",
    "package.json",
]

# TODO: Robots is currently broken.
generate_robots = false
# TODO: RSS not yet implemented.
generate_rss = false
generate_sitemap = true
+++

\newcommand{\unit}[1]{\! \text{#1}}

\newcommand{\blurb}[1]{@@blurb #1 @@}

\newcommand{\note}[1]{@@note @@title ⚙ Note@@ @@content #1 @@ @@}
\newcommand{\warn}[1]{@@warning @@title ⚠ Warning!@@ @@content #1 @@ @@}

<!-- TODO: Set the figure number automatically. -->
\newcommand{\figure}[2]{@@figure #1 @@caption #2 @@ @@}
