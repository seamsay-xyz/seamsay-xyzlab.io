+++
title = "What Scientists Should Know About Software Development"
tags = ["blog", "software", "research software engineering", "science"]
+++

# What Scientists Should Know About Software Development

\blurb{
I recently gave a talk on unit testing to some of the others in my centre, and the most shocking realisation I came away with was just how many software development concepts I take for granted.
So I wanted to to write up a little bit about some of the more important concepts that I think are useful to be aware of.
}

* Why care?
  * Reproducibility and replicability are the point of science.
  * Trustworthiness: bugs that invalidate their results are what keep computational scientists up at night.
  * Time spent working with bad code is time not spent doing science.
* This isn't supposed to be a comprehensive guide to any of these topics.

## Your Relationship To Your Own Code

### You Are A User

### You Are A Collaborator

## Version Control

## Unit Testing

## Purity And Global State

## Libraries And Packages

## How To Think About Errors

* Two types of errors: expected and unexpected.
* Errors Should Explain What Went Wrong

## Resources
