{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    utils,
  }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};
      buildInputs = with pkgs; [
        git
        julia-bin
        # TODO: Pre-install needed node packages (highlight.js, lunr, cheerio)
        nodejs
        (python3.withPackages (ps: with ps; [css-html-js-minify numpy scipy]))
      ];
    in {
      devShells.default = pkgs.mkShell {inherit buildInputs;};
      formatter = pkgs.alejandra;
    });
}
