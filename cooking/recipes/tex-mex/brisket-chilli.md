+++
title = "Brisket Chilli"
tags = ["recipe"]
+++

# Brisket Chilli Con Carne

\blurb{A chilli made with pulled beef instead of mince.}

## Ingredients

### For Cooking The Beef

* $20 \unit{g}$ butter.
* $1 \unit{tbsp}$ cooking oil.
* $800 \unit{g}$ beef brisket.
* $3$ medium brown onions, chopped roughly.
* $5$ garlic cloves, peeled but whole.
* $3 \unit{cm} \times 3 \unit{cm}$ ginger.
* $700 \unit{g}$ tomatoes (ideally plum or cherry tomatoes).
* $2 \unit{tsp}$ whole peppercorns.
* $4$ bay leaves.
* Whole dried chillies depending on desired spice level ($2$ is usually good for a mild dish).
* $3 \unit{tsp}$ paprika[^paprika].
* $1 \unit{tsp}$ ground cumin.
* $1 \unit{tsp}$ ground coriander.
* $0.5 \unit{tsp}$ garlic powder.
* $0.5 \unit{tsp}$ onion powder.
* $1 \unit{tsp}$ salt.
* $3 \unit{tsp}$ English mustard.

### For Everything Else

* $10 \unit{g}$ butter.
* $2 \unit{tbsp}$ cooking oil.
* $400 \unit{g}$ can of kidney beans, drained.
* $400 \unit{g}$ can of black beans, drained.
* $3$ garlic cloves, diced.
* $1$ large red onion, sliced relatively thinly ($\approx 2 \unit{mm}$ wide).
* $3$ peppers, sliced roughly as thickly as the red onion.
* Fresh chillis depending on desired spice level, deseeded and sliced into long thin strips.
* $3 \unit{tsp}$ paprika[^paprika].
* $1 \unit{tsp}$ ground cumin.
* $1 \unit{tsp}$ ground coriander.
* $1 \unit{tsp}$ salt.

## Recipe

1. A couple of hours prior to cooking, mix together the ground spices for the beef then coat the beef in the yellow mustard and rub on the spice mix.
   Leave this to marinate until you're ready to cook.
1. Preheat the oven to $160 \degree \! \text{C}$.
1. Melt the butter with the oil in a Dutch oven[^pans] on a medium-high heat, then sear the beef on all sides before removing from the pan and putting aside.
1. Add the brown onions and tomatoes, and cook for a few minutes until they start to caramelise slightly.
1. Add $50 \unit{ml}$ water or red wine, to deglaze the pan.
1. Return the beef to the pan and add the remaining ingredients for the beef, before placing the pan in the oven _without_ a lid.
1. After cooking for $30 \unit{minutes}$, add a lid to the pan and cook for $3 \unit{hours}$.
1. Blend together everything from the pan **except** the beef and bay leaves. Return the blended mixture to the pan and shred the beef (it should just fall apart at this point).
1. Heat the rest of the oil and butter in a different frying pan and fry the red onion with the peppers on a medium-high heat for about $10 \unit{minutes}$ or until the onions start to show signs of caramelisation[^onions].
1. Add the garlic and fresh chillis then fry for another $2 \unit{minutes}$.
1. Add the spices then turn the heat down low and cook for another $5 \unit{minutes}$.
1. Mix the onions and peppers into the beef mixture, add the beans, then simmer on a medium heat for $30 \unit{minutes}$ to cook the beans.

[^paprika]: I use a mix of hot and smoked paprika.
[^pans]: If you don't have a Dutch oven then ideally use an oven-proof stovetop pan, so that you can seal and cook the beef in the same pan.
[^onions]: You can start this process before the beef has finished cooking if you wish.
