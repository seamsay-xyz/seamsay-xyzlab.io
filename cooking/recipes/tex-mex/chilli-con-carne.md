+++
title = "Chilli"
tags = ["recipe"]
+++

# Chilli Con Carne

\blurb{
  My own take on what I believe was originally a Tex-Mex recipe, though this recipe is more similar to how it's cooked in the UK.
  This uses my own tomato-onion gravy to give a nice, thick sauce.
}

## Ingredients

* $1 \unit{tbsp}$ cooking oil.
* $1$ medium red onion, chopped into roughly $1 \unit{cm} \times 1 \unit{cm}$ squares.
* $2$ red, orange, or yellow peppers, chopped into roughly $1 \unit{cm} \times 1 \unit{cm}$ squares.
* $500 \unit{g}$ beef mince.
* $2$ cloves garlic, finely diced.
* $1 \unit{tsp}$ salt.
* $3 \unit{tsp}$ paprika (I use a mix of smoked and hot).
* $1 \unit{tsp}$ ground cumin.
* $0.5 \unit{tsp}$ ground coriander.
* A $400 \unit{g}$ can of kidney beans.
* Some kind of chilli (otherwise it would just be a carne...).
  This really depends on how spicy you like things and what spices you like.
  Some suggestions are:
    * Fresh chillies.
      Use two medium chillis for a mild dish, or try birds eye chillis for something hotter.
      Slice them into little circles and add them with the peppers.
    * Chilli powder.
      Mix it in with the spices.
    * Hot sauce.
      Add it when you add the gravy to the meat and veg.

### Gravy

* $1 \unit{tbsp}$ cooking oil.
* $1$ medium brown onion, chopped roughly.
* $300 \unit{g}$ tomatoes, the smaller the better.
* $3$ cloves garlic, sliced roughly.
* $1 \unit{tsp}$ salt.
* $6$ whole peppercorns.
* $1$ bay leaf.

## Recipe

1. For the gravy.
    1. Heat the oil on a medium-high heat in a lidded frying pan.
    1. Add the onion and fry for $5$ - $10 \unit{minutes}$, stirring occasionally, until the onions have started to caramelise.
    1. Add the tomatoes, garlic, salt, peppercorns, and bay leaf and fry for $3 \unit{minutes}$.
    1. Put the lid on and turn the heat down to low, cooking for at least $30 \unit{minutes}$.
       Stir every couple of minutes, just to make sure that nothing sticks.
       Ensure the heat is high enough for there to always be a slight sizzle (or boil once the juice comes through).
    1. Once the $30 \unit{minutes}$ is up (feel free to cook it for longer if you're still working on the rest of the recipe), take the bay leaf out (adding it to the meat and veg mix) and blend the rest with a stick blender.
1. While the gravy is cooking, heat the rest of the oil on a medium heat in another sauce pan.
1. Fry the onion for about $10 \unit{minutes}$ until the onion starts to go brown.
1. Add the peppers and fry for another $5$ - $10 \unit{minutes}$.
1. Add the garlic.
1. Add the mince bit by bit, breaking it up before you add it to the pan and making sure that you boil off any juice from the mince.
1. Mix the spices together in a little pot then add them to the mix, stirring thoroughly and letting them fry for a couple of minutes.
1. Add the blended gravy.
1. Add the kidney beans.
   If you want a relatively dry chilli (for putting in a tortilla, for example) then drain the kidney beans before adding them.
   Otherwise add the kidney beans with their juice.
1. Bring the mixture to a simmer and let it cook down to the consistency that you desire, cooking for at least $10 \unit{minutes}$.
   If the chilli is already at the desired consistency, add a lid to the sauce pan.
