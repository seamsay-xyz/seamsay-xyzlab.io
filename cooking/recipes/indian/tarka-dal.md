+++
title = "Tarka Dal"
tags = ["recipe", "curry", "vegetarian"]
+++

# Tarka Dal

\blurb{A light and healthy lentil curry, potentially somewhat drier than you're used to.}

## Ingredients

* $250 \unit{g}$ red lentils.
* $2 \unit{cm}^3$ ginger.
* $8$ fenugreek seeds.
* $0.5 \unit{tsp}$ salt.
* $1 \unit{tbsp}$ cooking oil.
* $1$ medium brown onion, diced finely.
* $1 \unit{tsp}$ yellow mustard seeds.
* $3$ cloves of garlic.
* $1 \unit{tbsp}$ tomato purée.
* $1 \unit{tbsp}$ lemon juice.

### Tarka

I use a very basic tarka for this recipe, but you can find far more involved ones online if you fancy something more extravagant.

* $2 \unit{tbsp}$ ghee or high quality oil (avoid olive oil if you can, as good olive oils can go bitter when used at high heat).
* $2 \unit{tsp}$ deggi mirch (or substitute $1 \unit{tsp}$ chilli powder and $1 \unit{tsp}$ paprika).
* $1 \unit{tsp}$ [garam masala](../garam-masala).

## Recipe

### Lentils

1. Bring lentils to the boil in $500 \unit{ml}$ of water.
1. Skim the scum that will form off from the top of lentils.
1. Add the fenugreek seeds, ginger, and salt.
1. Simmer for $30 \unit{minutes}$.

### Onions

This should be done when the lentils have about $10 \unit{minutes}$ left.

1. Heat the oil ($1 \unit{tbsp}$) on a high heat.
1. Bring the heat down to medium-high and fry the onions for $3 \unit{minutes}$.
1. Add the mustard seeds and fry until they begin to pop.
1. Add the garlic and fry for $1 \unit{minute}$.

### Bring It Together

1. Add the onions to the lentils.
1. Add the tomato puree, and lemon juice.
1. Simmer for $30 \unit{minutes}$, topping up with **small** amounts of water (literally just $1 \unit{tbsp}$ at a time, otherwise you run the risk of making it too soupy) if it starts to get too dry.

### Tarka

1. Heat the oil ($2 \unit{tbsp}$) on a high heat.
1. Add the deggi mirch and garam masala and fry for $20 \unit{seconds}$.

Finally, pour the tarka over the dal and serve!
