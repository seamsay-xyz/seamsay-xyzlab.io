+++
title = "Garam Masala"
tags = ["recipe", "spices"]
+++

# Garam Masala

\blurb{Warming spices that are a staple for many Indian recipes.}

## Ingredients

* $1 \unit{tbsp}$ cardamom seeds.
* $5 \unit{cm}$ stick of cinnamon.
* $1 \unit{tsp}$ cumin seeds.
* $1 \unit{tsp}$ cloves.
* $1 \unit{tsp}$ peppercorns.
* $\frac{1}{4}$ of a nutmeg, ground.
