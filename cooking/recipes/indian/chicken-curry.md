+++
title = "Chicken Curry"
tags = ["recipe", "curry"]
+++

# Chicken Curry

\blurb{The marinade for this recipe is based on the marinade for the Chicken Ruby recipe from the Dishoom cookbook, and the gravy recipe is my own concoction.}

**Serves**: 4 if used as the only dish, 8 if used alongside side dishes.

## Ingredients

* $1 \unit{kg}$ of chicken, chopped into large pieces ($4 \unit{cm}$ cubes, each piece should be more than one mouthful).
    * If using thighs, I usually cut them into two (three if the thigh is particularly large).
    * If using breast, I usually cut each one into four to six pieces.

### Marinade

* $2 \unit{cm}$ cube of ginger, roughly chopped.
* $5$ cloves of garlic, peeled.
* $2 \unit{tsp}$ salt.
* $1.5 \unit{tsp}$ deggi mirch.
    * If you can't find deggi mirch, substitute $1.5 \unit{tsp}$ paprika and $0.25 \unit{tsp}$ hot chilli powder.
* $2 \unit{tsp}$ ground cumin.
* $0.75 \unit{tsp}$ garam masala.
* Juice of $0.5$ limes.
* $1 \unit{tbsp}$ cooking oil.
* $100 \unit{g}$ Greek yoghurt.

### Gravy

* $2 \unit{tbsp}$ cooking oil.
* $6$ cardamom pods.
* $1$ medium cinnamon stick.
* $3$ medium onions, roughly chopped.
* $4$ cloves of garlic, crushed slightly under the flat edge of a knife and peeled (don't forget to cut the root off).
* $2 \unit{cm}$ cube of ginger, roughly chopped.
* $400 \unit{g}$ tomatoes, the smaller the better.
    * If using small tomatoes (e.g. cherry tomatoes), leave them whole.
    * If using medium tomatoes, cut them into quarters.
    * If using large tomatoes, cut the tomato in half then cut each half into quarters.
* $1 \unit{tsp}$ salt.
* $1 \unit{tsp}$ deggi mirch.
* $0.5 \unit{tsp}$ ground coriander.

## Recipe

1. At least $3 \unit{hrs}$ (or up to $24 \unit{hrs}$) prior to cooking blend all the marinade ingredients together and mix in with the chicken[^marinade].
1. Heat the oil in a large frying pan (or other suitable pan for cooking on the hob) on a medium-high heat.
1. Once the oil is up to temperature, add the cardamom pods and cinnamon sticks. Fry for $1 \unit{minute}$.
1. Add the onions, garlic, and ginger and fry for $5$ - $10 \unit{minutes}$ until the onions start to brown.
1. Add the tomatoes and fry for a further $3 \unit{minutes}$.
1. Mix in the salt, deggi mirch, and coriander.
1. Turn the heat down to medium-low and put a lid on the pan. Cook for at least $30 \unit{minutes}$, stirring every $5 \unit{minutes}$ or so.
1. Meanwhile place the chicken on a grill pan. Do *not* scrape off the marinade, as this is what will give the chicken a nice crispy texture.
1. Cook the chicken under a medium-hot grill.
    1. Cook it on one side until most of it is starting to brown, and some of it is starting to go black.
    1. Flip the chicken over and cook for another $5$ - $10 \unit{minutes}$.
1. Once cooked, blend the gravy and return it to the pan on a low heat. If it's too thick, add water (not too quickly, just a tablespoon or two at a time).
1. Add the chicken to the gravy.
1. Scrape out the remaining marinade from whatever container you stored it in and mix it in with the gravy and chicken.
1. If the gravy is too thick, mix in $50 \unit{ml}$ water. Repeat until desired consistency is achieved.

[^marinade]: If you want to do this more than $24 \unit{hrs}$ in advance, make sure you use fresh chicken and freeze it in the marinade (ensure you leave time to let it defrost).
