+++
title = "Margarita"
tags = ["recipes", "cocktails"]
+++

# Margarita

## Strong

\blurb{This is a strong margarita recipe that requires good quality alcohol.}

* $75 \unit{ml}$ good quality[^quality] 100% agave tequila blanco.
* $50 \unit{ml}$ Cointreau, or other strong orange liqueur[^orange].
* The juice from $1$ lime, freshly squeezed.
* $15 \unit{ml}$ light agave syrup.
* A small pinch of salt (don't include if you're going to rim[^rim] your glass).

Mix the ingredients in a cocktail shaker with an ice cube and pour.
If you haven't rimmed[^rim] your glass, sprinkle a small amount of smoked paprika on top.

## Standard

\blurb{A less potent margarita recipe in which you can get away with using lower quality alcohol.}

* $50 \unit{ml}$ tequila blanco.
* $25 \unit{ml}$ Triple Sec, or other low alcohol orange liqueur.
* $25 \unit{ml}$ lime juice, or the juice from $1$ freshly squeezed lime.

[^quality]: Don't be fooled into thinking that all 100% agave tequila is of high quality, it's not...
[^orange]: If using an orange liqueur with a lower ($\lt 30 \%$) alcohol content, consider omitting the agave syrup to avoid making the drink too sickly.
[^rim]: 😏
