+++
title = "(Elderflower) Gin Tommy Lee Collins"
tags = ["recipes", "cocktails"]
+++

# (Elderflower) Gin Tommy Lee Collins

\blurb{A Tom Collins variant invented by a friend of mine.}

* $35 \unit{ml}$ (elderflower) gin.
* $15 \unit{ml}$ sugar syrup.
* The juice from $\frac{1}{2}$ a freshly squeezed lemon.
* $15 \unit{ml}$ ginger wine.
* $3$-$5$ dashes of orange and mandarin bitters.