+++
title = "Caipirinha"
tags = ["recipes", "cocktails"]
+++

# Caipirinha

* $50 \unit{ml}$ cachaça.
* $50 \unit{ml}$ dark rum.
* $1 \unit{tbsp}$ light brown sugar.
* $\frac{1}{2}$ of a lime, cut into $4$ wedges.

1. Muddle the lime and sugar together until the sugar is fully dissolved.
1. Add the cachaça and rum.
1. Mix thoroughly.
1. Add ice.