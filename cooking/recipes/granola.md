+++
title = "Granola"
tags = ["recipes", "breakfast"]
+++

# Granola

\blurb{A slightly adapted granola recipe given to me by a friend, though I'm not sure of the original source.}

## Nutritional Information

The following ingredients will make $500 \unit{g}$ of granola, with $430 \unit{kcal}$ per $100 \unit{g}$.

## Ingredients

### Dry

* $225 \unit{g}$ jumbo porridge oats.
* $150 \unit{g}$ mixed seeds, nuts, and dried fruit.
* $1 \unit{tsp}$ ground ginger.
* $\frac{1}{2} \unit{tsp}$ ground cinnamon.
* $3$ cloves, ground.
* $\frac{1}{8} \unit{tsp}$ ground nutmeg.
* $\frac{1}{2} \unit{tsp}$ salt.

### Wet

* $30 \unit{g}$ golden syrup (or honey).
* $60 \unit{g}$ dark agave syrup (or maple syrup).
* $20 \unit{g}$ olive oil.
* $50 \unit{g}$ light brown sugar.

## Recipe

1. Mix dry ingredients into a large bowl.
1. Add the wet ingredients to a small pan and heat gently until the mix becomes thin enough to stir easily.
1. Combine the wet ingredients with the dry ingredients and mix well.
1. Pour mixture into a baking tray.
   The baking tray should ideally be large enough that the layer of granola mix is not too thick (approximately $1$-$2 \unit{cm}$), otherwise your granola will not become crunchy enough[^tray].
1. Bake at $160 \, {}^\circ \text{C}$ for $20 \unit{minutes}$.
1. Mix the granola around then put it back in the oven for another $15 \unit{minutes}$.
1. When the granola finishes cooking **do not mix it a second time**[^mixing], instead allow it to cool undisturbed.
1. Once cool knock the granola out of the pan and put it into an airtight container for storage.

[^tray]: Don't panic if your pan isn't big enough, it will still taste good!
[^mixing]: Mixing it a second time while hot will prevent clumps from forming.